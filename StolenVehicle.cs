﻿using GTA;
using System;

namespace GrandTheftAuto
{
    class StolenVehicle
    {
        private Random rand = new Random();

        public Vehicle vehicle;
        /// <summary>
        /// true called, false found
        /// </summary>
        public bool calledOrFound;
        public Ped[] possibleCallers;
        public bool possibleCallersHaveCalled;
        public int timePoliceStartLooking;
        public int timePoliceStopLooking;
        public bool policeJustStartedLooking;

        private int timeOffset;

        public StolenVehicle(Vehicle Vehicle, bool CalledOrFound, Ped[] PossibleCallers)
        {
            vehicle = Vehicle;
            calledOrFound = CalledOrFound;
            possibleCallers = PossibleCallers;
            possibleCallersHaveCalled = false;
            timeOffset = 0;
            GenerateTimes(calledOrFound, false);
            policeJustStartedLooking = false;
        }

        public void GenerateTimes(bool CalledOrFound, bool NewTimes)
        {
            int newTimeOffset = 0;

            if (calledOrFound)
                newTimeOffset = INI.settings.timeTakePedCall + rand.Next(-1 * INI.settings.pedCallTimeRandAmount, INI.settings.pedCallTimeRandAmount);
            else
                newTimeOffset = INI.settings.timeTakeNoticeStolenCar + rand.Next(-1 * INI.settings.noticeStolenVehicleTimeRandAmount, INI.settings.noticeStolenVehicleTimeRandAmount);

            timePoliceStartLooking = Game.GameTime + (NewTimes ? newTimeOffset - timeOffset : newTimeOffset);
            timePoliceStopLooking = timePoliceStartLooking + (calledOrFound ? INI.settings.timePoliceSearchPedCall : INI.settings.timePoliceSearchNoticedStolenVehicle);

            timeOffset = newTimeOffset;
        }
    }
}
