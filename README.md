A mod that shares the name of the game it's made for. Great.

Makes it so that when you steal vehicles, people around you will call the cops on you.
If you kill everyone around you, or there wasn't anyone around to begin with, the police will be notified of a missing vehicle in a few minutes.