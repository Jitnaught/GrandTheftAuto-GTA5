﻿using GTA;
using System.Collections.Generic;

namespace GrandTheftAuto
{
    static class Extensions
    {
        public static bool ContainsVehicle(this List<StolenVehicle> stolenVehs, Vehicle veh)
        {
            for (int i = 0; i < stolenVehs.Count; i++)
            {
                if (stolenVehs[i].vehicle == veh) return true;
            }

            return false;
        }
    }
}
