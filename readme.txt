Yes, a mod for Grand Theft Auto V called Grand Theft Auto.

With this mod, when you steal a vehicle, pedestrians around you will report you to the cops. If there are no pedestrians (or they are dead within around 15 seconds), then a missing vehicle will be reported in around 5 minutes.
When the police have been notified, they will search for you. If they see you and fully recognize that it is you, you will get a wanted level.

More on how the mod works in the howitworks.txt file.

Installation:
* Install Script Hook V and Script Hook V .NET (make sure you have created a scripts folder in your GTA V directory).
* Extract the GrandTheftAuto.dll and GrandTheftAuto.ini files into the scripts folder in your GTA V directory.

Settings:
* CALLERS_SEARCH_RADIUS -> the radius of which callers will be able to see you. larger values will take more time
* POLICE_SEARCH_RADIUS -> the radius of which cops will be able to see you. larger values will take more time
* DISABLE_WHILE_IN_MISSION -> if true, will disable the mod while in a mission
* COPS_STOP_LOOKING_WHEN_ALREADY_WANTED -> if true, cops will stop looking for you when you have a wanted level
* STARS_GET_WHEN_PED_CALL -> stars to get when police find you after being reported by someone who saw the GTA
* STARS_GET_WHEN_NOTICE_STOLEN_VEHICLE -> stars to get when police find you after being reported by someone who noticed the missing vehicle
* RECEIVE_ADDITIONAL_STAR_WHEN_ALREADY_WANTED -> whether or not to get an additional star when found with the stolen vehicle while already wanted
* MAX_ADDITIONAL_STARS -> the max wanted level when receiving additional stars
// all times and rand amounts are in milliseconds
* TIME_TAKE_FOR_PED_TO_CALL -> time it takes for possible callers to report you
* TIME_TAKE_TO_NOTICE_MISSING_VEHICLE -> time it takes for someone to notice the missing vehicle
* HOW_LONG_POLICE_SEARCH_FOR_PED_CALL -> how long police will search for you when someone reported the GTA
* HOW_LONG_POLICE_SEARCH_FOR_NOTICED_STOLEN_VEHICLE -> how long police will search for you when someone reports the missing vehicle
* RANDOMIZATION_AMOUNT_FOR_TIME_TAKE_PED_TO_CALL -> will randomize TIME_TAKE_FOR_PED_TO_CALL by the set amount of milliseconds. example: if you set the randomization to 2000, and TIME_TAKE_FOR_PED_TO_CALL to 5000, then TIME_TAKE_FOR_PED_TO_CALL will be randomized from (5000 - 2000) to (5000 + 2000)
* RANDOMIZATION_AMOUNT_FOR_TIME_TAKE_TO_NOTICE_MISSING_VEHICLE -> same as above but for NOTICE_MISSING_VEHICLE instead
* SHOW_COPS_HAVE_BEEN_CALLED_MESSAGE -> whether or not to show message when the cops are called
* SHOW_COPS_FOUND_YOU_MESSAGE -> whether or not to show message when the police find you
* SHOW_WANTED_ALREADY_COPS_FOUND_YOU_MESSAGE -> whether or not to show message when the cops find you when you already have a wanted level
* SHOW_COPS_STOPPED_LOOKING_MESSAGE -> whether or not to show message when the police have stopped looking for you
* SHOW_COPS_SAW_YOU_STEALING_MESSAGE -> whether or not to show message when the police see you stealing a vehicle
* SHOW_LOST_COPS_MESSAGE -> whether or not to show message when you lose your wanted level
* COPS_HAVE_BEEN_CALLED_MESSAGE -> message to show when police have been called
* COPS_FOUND_YOU_MESSAGE -> message to show when police have found you
* WANTED_ALREADY_COPS_FOUND_YOU_MESSAGE -> message to show when police have found you when you already have a wanted level
* COPS_STOPPED_LOOKING_MESSAGE -> message to show when police have stopped looking for you
* COPS_SAW_YOU_STEALING_MESSAGE -> message to show when police see you stealing a vehicle
* LOST_COPS_MESSAGE -> message to show when you've lost your wanted level
* SHOW_RECOGNITION_MESSAGE -> whether to show a little message by your minimap that shows how long you've been seen by the cops
* RECOGNITION_MESSAGE_SCALE -> how big the that message will be
* RECOGNITION_MESSAGE_POSITION_X -> the X position of that message
* RECOGNITION_MESSAGE_POSITION_Y -> the Y position of that message

Known issues:
* There may be some lag.
* Some vehicles that aren't stolen may be picked up, and some that are stolen may not be picked up.

Change-log:
v1.0.1:
	Fixed changing the search radius settings not having any effect.

See the source code on GitHub! https://github.com/Jitnaught/GrandTheftAuto_Mod

Credits:
* destroyer11687 (a.k.a. /u/NuttyWompRat of Reddit) for requesting the mod. (https://www.reddit.com/r/GTAV_Mods/comments/5745mj/would_anybody_be_willing_to_teach_me_how_to/)
* Jitnaught for writing the script.