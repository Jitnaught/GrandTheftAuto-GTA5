﻿using GTA;

namespace GrandTheftAuto
{
    class INI
    {
        public class INISettings
        {
            private const string INI_SECTION_SETTINGS = "SETTINGS", INI_SECTION_STARS = "STARS", INI_SECTION_TIMES = "TIMES", 
                INI_SECTION_MESSAGE_TOGGLES = "TOGGLE_MESSAGES", INI_SECTION_MESSAGES = "MESSAGES", 
                INI_SECTION_RECOGNITION = "RECOGNITION", INI_SECTION_RECOGNITION_MESSAGE = "RECOGNITION_MESSAGE";

            private const string INI_KEY_CALLERS_SEARCH_RADIUS = "CALLERS_SEARCH_RADIUS",
                INI_KEY_POLICE_SEARCH_RADIUS = "POLICE_SEARCH_RADIUS",
                INI_KEY_DISABLE_IN_MISSION = "DISABLE_WHILE_IN_MISSION",
                INI_KEY_COPS_STOP_LOOKING_WHEN_WANTED = "COPS_STOP_LOOKING_WHEN_ALREADY_WANTED",

                INI_KEY_STARS_ON_CALL = "STARS_GET_WHEN_PED_CALL",
                INI_KEY_STARS_ON_NOTICE_STOLEN_CAR = "STARS_GET_WHEN_NOTICE_STOLEN_VEHICLE",
                INI_KEY_RECEIVE_ADDITIONAL_STAR_WHEN_ALREADY_WANTED = "RECEIVE_ADDITIONAL_STAR_WHEN_ALREADY_WANTED",
                INI_KEY_MAX_ADDITIONAL_STARS = "MAX_ADDITIONAL_STARS",

                INI_KEY_TIME_TAKE_PED_CALL = "TIME_TAKE_FOR_PED_TO_CALL",
                INI_KEY_TIME_TAKE_NOTICE_STOLEN_VEHICLE = "TIME_TAKE_TO_NOTICE_MISSING_VEHICLE",
                INI_KEY_TIME_POLICE_SEARCH_PED_CALL = "HOW_LONG_POLICE_SEARCH_FOR_PED_CALL",
                INI_KEY_TIME_POLICE_SEARCH_NOTICED_STOLEN_VEHICLE = "HOW_LONG_POLICE_SEARCH_FOR_NOTICED_STOLEN_VEHICLE",
                INI_KEY_PED_CALL_RAND_AMOUNT = "RANDOMIZATION_AMOUNT_FOR_TIME_TAKE_PED_TO_CALL",
                INI_KEY_NOTICE_STOLEN_VEHICLE_RAND_AMOUNT = "RANDOMIZATION_AMOUNT_FOR_TIME_TAKE_TO_NOTICE_MISSING_VEHICLE",

                INI_KEY_SHOW_EXTRA_INFO_MESSAGES = "SHOW_EXTRA_INFO_MESSAGES",
                INI_KEY_SHOW_COPS_CALLED_MESSAGE = "SHOW_COPS_HAVE_BEEN_CALLED_MESSAGE",
                INI_KEY_SHOW_COPS_FOUND_YOU_MESSAGE = "SHOW_COPS_FOUND_YOU_MESSAGE",
                INI_KEY_SHOW_WANTED_ALREADY_COPS_FOUND_YOU_MESSAGE = "SHOW_WANTED_ALREADY_COPS_FOUND_YOU_MESSAGE",
                INI_KEY_SHOW_COPS_STOPPED_LOOKING_MESSAGE = "SHOW_COPS_STOPPED_LOOKING_MESSAGE",
                INI_KEY_SHOW_COPS_SAW_YOU_STEALING_MESSAGE = "SHOW_COPS_SAW_YOU_STEALING_MESSAGE",
                INI_KEY_SHOW_LOST_COPS_MESSAGE = "SHOW_LOST_COPS_MESSAGE",

                INI_KEY_COPS_CALLED_MESSAGE = "COPS_HAVE_BEEN_CALLED_MESSAGE",
                INI_KEY_COPS_FOUND_YOU_MESSAGE = "COPS_FOUND_YOU_MESSAGE",
                INI_KEY_WANTED_ALREADY_COPS_FOUND_YOU_MESSAGE = "WANTED_ALREADY_COPS_FOUND_YOU_MESSAGE",
                INI_KEY_COPS_STOPPED_LOOKING_MESSAGE = "COPS_STOPPED_LOOKING_MESSAGE",
                INI_KEY_COPS_SAW_YOU_STEALING_MESSAGE = "COPS_SAW_YOU_STEALING_MESSAGE",
                INI_KEY_LOST_COPS_MESSAGE = "LOST_COPS_MESSAGE",

                INI_KEY_GRADUAL_RECOGNITION = "GRADUAL_POLICE_RECOGINITION",
                INI_KEY_TIME_TAKE_RECOGNIZE = "TIME_TAKE_POLICE_RECOGNIZE",
                
                INI_KEY_SHOW_RECOGNITION_MESSAGE = "SHOW_RECOGNITION_MESSAGE",
                INI_KEY_RECOGNITION_MESSAGE_SCALE = "RECOGNITION_MESSAGE_SCALE",
                INI_KEY_RECOGNITION_MESSAGE_POS_X = "RECOGNITION_MESSAGE_POSITION_X",
                INI_KEY_RECOGNITION_MESSAGE_POS_Y = "RECOGNITION_MESSAGE_POSITION_Y";

            private const int defaultCallersSearchRadius = 150, defaultPoliceSearchRadius = 100, defaultStarsOnCall = 1, 
                defaultStarsOnNoticeStolenCar = 1, defaultMaxAdditionalStars = 4, defaultTimeTakePedCall = 15000, defaultTimeTakeNoticeStolenCar = 60000 * 5,
                defaultTimePoliceSearchPedCall = 60000 * 5, defaultTimePoliceSearchNoticedStolenVehicle = 60000 * 5,
                defaultPedCallTimeRandAmount = 2000, defaultNoticeStolenVehicleTimeRandAmount = 60000 * 2,
                defaultTimeTakeRecognize = 1250, defaultRecognitionMessagePosX = 210, defaultRecognitionMessagePosY = 700;

            private const float defaultRecognitionMessageScale = 0.3f;

            private const bool defaultDisableInMission = true, defaultCopsStopLookingWhenWanted = false, defaultReceiveAdditionalStarWhenWanted = true, defaultShowExtraInfoMessages = false,
                defaultShowCopsCalledMessage = true, defaultShowCopsFoundYouMessage = true, defaultShowWantedAlreadyCopsFoundYouMessage = true, defaultShowCopsStoppedLookingMessage = true, 
                defaultShowCopsSawYouStealingMessage = true, defaultShowLostCopsMessage = true, defaultGradualRecognition = true, defaultShowRecognitionMessage = false;

            private const string defaultCopsCalledMessage = "The vehicle you stole has been reported to the police",
                defaultCopsFoundYouMessage = "You are now wanted because the police were notified of a stolen vehicle",
                defaultWantedAlreadyCopsFoundYouMessage = "Your wanted level has increased because you stole a vehicle",
                defaultCopsStoppedLookingMessage = "The police have stopped looking for you",
                defaultCopsSawYouStealingMessage = "The police saw you stealing that vehicle!",
                defaultLostCopsMessage = "The police are still looking for you";

            public int callersSearchRadius, policeSearchRadius, starsOnCall, starsOnNoticeStolenCar, maxAdditionalStars,
                timeTakePedCall, timeTakeNoticeStolenCar, timePoliceSearchPedCall, timePoliceSearchNoticedStolenVehicle,
                pedCallTimeRandAmount, noticeStolenVehicleTimeRandAmount, timeTakeRecognize, recognitionMessagePosX, recognitionMessagePosY;

            public float recognitionMessageScale = 1.0f;

            public bool disableInMission, copsStopLookingWhenWanted, receiveAdditionalStarWhenWanted, showExtraInfoMessages, showCopsCalledMessage, showCopsFoundYouMessage,
                showWantedAlreadyCopsFoundYouMessage, showCopsStoppedLookingMessage, showCopsSawYouStealingMessage, showLostCopsMessage, gradualPoliceRecognition, showRecognitionMessage;

            public string copsCalledMessage, copsFoundYouMessage, wantedAlreadyCopsFoundYouMessage, copsStoppedLookingMessage, copsSawYouStealingMessage, lostCopsMessage;

            private ScriptSettings _scriptSettings;

            private int getINIInt(string section, string key, int defaultValue)
            {
                int iTemp = _scriptSettings.GetValue(section, key, defaultValue);
                if (iTemp < 0) iTemp = defaultValue;

                return iTemp;
            }

            internal INISettings(ScriptSettings scriptSettings)
            {
                _scriptSettings = scriptSettings;
                
                callersSearchRadius = getINIInt(INI_SECTION_SETTINGS, INI_KEY_CALLERS_SEARCH_RADIUS, defaultCallersSearchRadius);
                policeSearchRadius = getINIInt(INI_SECTION_SETTINGS, INI_KEY_POLICE_SEARCH_RADIUS, defaultPoliceSearchRadius);
                disableInMission = scriptSettings.GetValue(INI_SECTION_SETTINGS, INI_KEY_DISABLE_IN_MISSION, defaultDisableInMission);
                copsStopLookingWhenWanted = scriptSettings.GetValue(INI_SECTION_SETTINGS, INI_KEY_COPS_STOP_LOOKING_WHEN_WANTED, defaultCopsStopLookingWhenWanted);

                starsOnCall = getINIInt(INI_SECTION_STARS, INI_KEY_STARS_ON_CALL, defaultStarsOnCall);
                starsOnNoticeStolenCar = getINIInt(INI_SECTION_STARS, INI_KEY_STARS_ON_NOTICE_STOLEN_CAR, defaultStarsOnNoticeStolenCar);
                receiveAdditionalStarWhenWanted = scriptSettings.GetValue(INI_SECTION_STARS, INI_KEY_RECEIVE_ADDITIONAL_STAR_WHEN_ALREADY_WANTED, defaultReceiveAdditionalStarWhenWanted);
                maxAdditionalStars = getINIInt(INI_SECTION_STARS, INI_KEY_MAX_ADDITIONAL_STARS, defaultMaxAdditionalStars);
                if (maxAdditionalStars > 5) maxAdditionalStars = defaultMaxAdditionalStars;

                timeTakePedCall = getINIInt(INI_SECTION_TIMES, INI_KEY_TIME_TAKE_PED_CALL, defaultTimeTakePedCall);
                timeTakeNoticeStolenCar = getINIInt(INI_SECTION_TIMES, INI_KEY_TIME_TAKE_NOTICE_STOLEN_VEHICLE, defaultTimeTakeNoticeStolenCar);
                timePoliceSearchPedCall = getINIInt(INI_SECTION_TIMES, INI_KEY_TIME_POLICE_SEARCH_PED_CALL, defaultTimePoliceSearchPedCall);
                timePoliceSearchNoticedStolenVehicle = getINIInt(INI_SECTION_TIMES, INI_KEY_TIME_POLICE_SEARCH_NOTICED_STOLEN_VEHICLE, defaultTimePoliceSearchNoticedStolenVehicle);
                pedCallTimeRandAmount = getINIInt(INI_SECTION_TIMES, INI_KEY_PED_CALL_RAND_AMOUNT, defaultPedCallTimeRandAmount);
                noticeStolenVehicleTimeRandAmount = getINIInt(INI_SECTION_TIMES, INI_KEY_NOTICE_STOLEN_VEHICLE_RAND_AMOUNT, defaultNoticeStolenVehicleTimeRandAmount);

                showExtraInfoMessages = scriptSettings.GetValue(INI_SECTION_MESSAGE_TOGGLES, INI_KEY_SHOW_EXTRA_INFO_MESSAGES, defaultShowExtraInfoMessages);
                showCopsCalledMessage = scriptSettings.GetValue(INI_SECTION_MESSAGE_TOGGLES, INI_KEY_SHOW_COPS_CALLED_MESSAGE, defaultShowCopsCalledMessage);
                showCopsFoundYouMessage = scriptSettings.GetValue(INI_SECTION_MESSAGE_TOGGLES, INI_KEY_SHOW_COPS_FOUND_YOU_MESSAGE, defaultShowCopsFoundYouMessage);
                showWantedAlreadyCopsFoundYouMessage = scriptSettings.GetValue(INI_SECTION_MESSAGE_TOGGLES, INI_KEY_SHOW_WANTED_ALREADY_COPS_FOUND_YOU_MESSAGE, defaultShowWantedAlreadyCopsFoundYouMessage);
                showCopsStoppedLookingMessage = scriptSettings.GetValue(INI_SECTION_MESSAGE_TOGGLES, INI_KEY_SHOW_COPS_STOPPED_LOOKING_MESSAGE, defaultShowCopsStoppedLookingMessage);
                showCopsSawYouStealingMessage = scriptSettings.GetValue(INI_SECTION_MESSAGE_TOGGLES, INI_KEY_SHOW_COPS_SAW_YOU_STEALING_MESSAGE, defaultShowCopsSawYouStealingMessage);
                showLostCopsMessage = scriptSettings.GetValue(INI_SECTION_MESSAGE_TOGGLES, INI_KEY_SHOW_LOST_COPS_MESSAGE, defaultShowLostCopsMessage);

                copsCalledMessage = scriptSettings.GetValue(INI_SECTION_MESSAGES, INI_KEY_COPS_CALLED_MESSAGE, defaultCopsCalledMessage);
                copsFoundYouMessage = scriptSettings.GetValue(INI_SECTION_MESSAGES, INI_KEY_COPS_FOUND_YOU_MESSAGE, defaultCopsFoundYouMessage);
                wantedAlreadyCopsFoundYouMessage = scriptSettings.GetValue(INI_SECTION_MESSAGES, INI_KEY_WANTED_ALREADY_COPS_FOUND_YOU_MESSAGE, defaultWantedAlreadyCopsFoundYouMessage);
                copsStoppedLookingMessage = scriptSettings.GetValue(INI_SECTION_MESSAGES, INI_KEY_COPS_STOPPED_LOOKING_MESSAGE, defaultCopsStoppedLookingMessage);
                copsSawYouStealingMessage = scriptSettings.GetValue(INI_SECTION_MESSAGES, INI_KEY_COPS_SAW_YOU_STEALING_MESSAGE, defaultCopsSawYouStealingMessage);
                lostCopsMessage = scriptSettings.GetValue(INI_SECTION_MESSAGES, INI_KEY_LOST_COPS_MESSAGE, defaultLostCopsMessage);

                gradualPoliceRecognition = scriptSettings.GetValue(INI_SECTION_RECOGNITION, INI_KEY_GRADUAL_RECOGNITION, defaultGradualRecognition);
                timeTakeRecognize = getINIInt(INI_SECTION_RECOGNITION, INI_KEY_TIME_TAKE_RECOGNIZE, defaultTimeTakeRecognize);

                showRecognitionMessage = scriptSettings.GetValue(INI_SECTION_RECOGNITION_MESSAGE, INI_KEY_SHOW_RECOGNITION_MESSAGE, defaultShowRecognitionMessage);
                recognitionMessageScale = scriptSettings.GetValue(INI_SECTION_RECOGNITION_MESSAGE, INI_KEY_RECOGNITION_MESSAGE_SCALE, defaultRecognitionMessageScale);
                if (recognitionMessageScale <= 0) recognitionMessageScale = defaultRecognitionMessageScale;
                recognitionMessagePosX = getINIInt(INI_SECTION_RECOGNITION_MESSAGE, INI_KEY_RECOGNITION_MESSAGE_POS_X, defaultRecognitionMessagePosX);
                recognitionMessagePosY = getINIInt(INI_SECTION_RECOGNITION_MESSAGE, INI_KEY_RECOGNITION_MESSAGE_POS_Y, defaultRecognitionMessagePosY);
            }
        }

        public static INISettings settings;

        public static void Initialize(ScriptSettings scriptSettings)
        {
            settings = new INISettings(scriptSettings);
        }
    }
}
