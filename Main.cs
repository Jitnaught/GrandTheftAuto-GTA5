﻿using GTA;
using GTA.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace GrandTheftAuto
{
    public class GrandTheftAuto : Script
    {
        private int[] copsPedTypes = { 6, 27 }; //6 = police, 27 = swat

        internal static List<StolenVehicle> stolenVehs = new List<StolenVehicle>();
        
        internal static int timeWithinDistanceCops = 0;
        private bool wasInMission = false, wasWanted = false;
        private object objTimerLock = new object();

        public GrandTheftAuto()
        {
            INI.Initialize(Settings);
            Mission.Initialize();

            Interval = 250;
            Tick += GrandTheftAuto_Tick;
        }

        private bool isStealingVehicle(Ped ped, out Vehicle outVehStolen, out Ped outDriver)
        {
            outVehStolen = null;
            outDriver = null;

            Ped pedLastInDriverSeat = null;

            if ((ped.IsTryingToEnterALockedVehicle || ped.IsGettingIntoAVehicle) &&
                (outVehStolen = ped.GetVehicleIsTryingToEnter()) != null && outVehStolen.Exists() && !outVehStolen.IsPersistent) //!ispersistent means not player's vehicle
            {
                if ((outVehStolen.NeedsToBeHotwired && outVehStolen.EngineHealth > 0)
                    || (!outVehStolen.IsSeatFree(VehicleSeat.Driver) && (outDriver = outVehStolen.Driver) != null)  //needs to be hotwired or a ped is already in driver seat
                    || ((pedLastInDriverSeat = Function.Call<Ped>(Hash.GET_LAST_PED_IN_VEHICLE_SEAT, outVehStolen, (int)VehicleSeat.Driver)) != null  //last ped is dead or is not me
                        && pedLastInDriverSeat.Exists() && (!pedLastInDriverSeat.IsAlive || pedLastInDriverSeat != ped)))
                    return true;
            }

            outVehStolen = null;
            outDriver = null;

            return false;
        }

        private IEnumerable<Ped> getNearbyHumansLookingAtEntity(Ped me, Entity entityLookingAt, float searchRadius, int[] pedTypes = null) //-1 to get all peds
        {
            Yield();

            Ped[] nearbyPeds = World.GetNearbyPeds(entityLookingAt.Position, searchRadius);

            for (int i = 0; i < nearbyPeds.Length; i++)
            {
                Yield();

                if (nearbyPeds[i] != null && nearbyPeds[i].Exists() && nearbyPeds[i] != me &&
                    nearbyPeds[i].IsAlive && nearbyPeds[i].IsHuman &&
                    (pedTypes == null || pedTypes.Contains(Function.Call<int>(Hash.GET_PED_TYPE, nearbyPeds[i]))) &&
                    !nearbyPeds[i].IsCuffed && !nearbyPeds[i].IsDiving && !nearbyPeds[i].IsFalling &&
                    !nearbyPeds[i].IsJacking && !nearbyPeds[i].IsRagdoll)
                {
                    Yield();
                    if (Function.Call<bool>(Hash.HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT, nearbyPeds[i], entityLookingAt))
                    {
                        Yield();
                        yield return nearbyPeds[i];
                    }
                }
            }
        }

        private void processStolenVehicles()
        {
            if (!Monitor.TryEnter(objTimerLock)) return; //if last tick still in progress then don't go further

            if (Game.MissionFlag)
            {
                if (!wasInMission)
                {
                    stolenVehs.Clear();
                    timeWithinDistanceCops = 0;

                    wasInMission = true;
                }
            }
            else if (wasInMission && Mission.DidMissionEnd)
            {
                wasInMission = false;

                stolenVehs.Clear();
            }

            if (!wasInMission || !INI.settings.disableInMission)
            {
                Player plr = Game.Player;
                Ped plrPed = plr.Character;

                if (plrPed.Exists() && plrPed.IsAlive)
                {
                    if (stolenVehs.Count > 0)
                    {
                        var nearbyCops = getNearbyHumansLookingAtEntity(plrPed, plrPed, INI.settings.policeSearchRadius, copsPedTypes);
                        bool copsSeeYou = nearbyCops.Any();
                        bool showLostCopsMessage = (INI.settings.showLostCopsMessage || INI.settings.showExtraInfoMessages) && wasWanted && plr.WantedLevel == 0;
                        wasWanted = plr.WantedLevel > 0;

                        for (int i = 0; i < stolenVehs.Count; i++)
                        {
                            Yield();

                            if (!stolenVehs[i].calledOrFound && stolenVehs[i].vehicle == null || !stolenVehs[i].vehicle.Exists())
                            {
                                stolenVehs.RemoveAt(i);
                                i--;
                                continue;
                            }

                            if (Game.GameTime > stolenVehs[i].timePoliceStartLooking)
                            {
                                if (Game.GameTime > stolenVehs[i].timePoliceStopLooking)
                                {
                                    if (stolenVehs.Count == 1 && (INI.settings.showCopsStoppedLookingMessage || INI.settings.showExtraInfoMessages)) UI.Notify(INI.settings.copsStoppedLookingMessage);

                                    stolenVehs.RemoveAt(i);
                                    i--;
                                    continue;
                                }

                                if (plr.WantedLevel > 0 && INI.settings.copsStopLookingWhenWanted)
                                {
                                    stolenVehs.RemoveAt(i);
                                    i--;
                                    continue;
                                }

                                //check if possible callers have been neutralized when police will first be called
                                if (!stolenVehs[i].policeJustStartedLooking)
                                {
                                    stolenVehs[i].policeJustStartedLooking = true;

                                    if (stolenVehs[i].calledOrFound)
                                    {
                                        if (!stolenVehs[i].possibleCallersHaveCalled)
                                        {
                                            for (int j = 0; j < stolenVehs[i].possibleCallers.Length; j++)
                                            {
                                                Yield();

                                                if (stolenVehs[i].possibleCallers[j] != null && stolenVehs[i].possibleCallers[j].Exists())
                                                {
                                                    if (stolenVehs[i].possibleCallers[j].IsAlive)
                                                    {
                                                        stolenVehs[i].possibleCallersHaveCalled = true;
                                                    }

                                                    stolenVehs[i].possibleCallers[j].IsPersistent = false;
                                                }
                                            }
                                        }

                                        if (!stolenVehs[i].possibleCallersHaveCalled) //if they haven't been called, generate new time for a non-call
                                        {
                                            if (INI.settings.showExtraInfoMessages) UI.Notify("Nobody called, police will notice eventually");

                                            stolenVehs[i].GenerateTimes(false, true);
                                            stolenVehs[i].policeJustStartedLooking = false;

                                            break; //don't need to check if police have seen you since we just generated new start time
                                        }
                                        else if (INI.settings.showCopsCalledMessage || INI.settings.showExtraInfoMessages) UI.Notify(INI.settings.copsCalledMessage);
                                    }
                                    else if (INI.settings.showCopsCalledMessage || INI.settings.showExtraInfoMessages) UI.Notify(INI.settings.copsCalledMessage);
                                }

                                //check if any police see you
                                if (copsSeeYou)
                                {
                                    if (stolenVehs[i].calledOrFound || plrPed.Position.DistanceTo(stolenVehs[i].vehicle.Position) < 50)
                                    {
                                        timeWithinDistanceCops += 250;

                                        if (!INI.settings.gradualPoliceRecognition || timeWithinDistanceCops >= INI.settings.timeTakeRecognize)
                                        {
                                            timeWithinDistanceCops = 0;

                                            bool setWantedLevel = ((stolenVehs[i].calledOrFound && plr.WantedLevel < INI.settings.starsOnCall) || (!stolenVehs[i].calledOrFound && plr.WantedLevel < INI.settings.starsOnNoticeStolenCar));
                                            bool additionalStar = INI.settings.receiveAdditionalStarWhenWanted && plr.WantedLevel > 0;

                                            if (setWantedLevel)
                                            {
                                                if (INI.settings.showCopsFoundYouMessage || INI.settings.showExtraInfoMessages) UI.Notify(INI.settings.copsFoundYouMessage);

                                                if (stolenVehs[i].calledOrFound)
                                                    plr.WantedLevel = INI.settings.starsOnCall;
                                                else
                                                    plr.WantedLevel = INI.settings.starsOnNoticeStolenCar;
                                            }
                                            else if (additionalStar)
                                            {
                                                if (INI.settings.showWantedAlreadyCopsFoundYouMessage || INI.settings.showExtraInfoMessages) UI.Notify(INI.settings.wantedAlreadyCopsFoundYouMessage);

                                                if (plr.WantedLevel < INI.settings.maxAdditionalStars) plr.WantedLevel++;
                                            }

                                            stolenVehs.Clear();

                                            timeWithinDistanceCops = 0;

                                            return; //don't need to check if vehicle is stolen right now
                                        }
                                    }
                                }
                                else
                                {
                                    if (showLostCopsMessage)
                                    {
                                        UI.Notify(INI.settings.lostCopsMessage);
                                        showLostCopsMessage = false;
                                    }

                                    if (timeWithinDistanceCops > 150) timeWithinDistanceCops -= 150;
                                    else timeWithinDistanceCops = 0;
                                }
                            }
                            else if (stolenVehs[i].calledOrFound) //before time when possible callers call
                            {
                                bool called = true; //have to check if callers have been killed before time to call because they might despawn

                                for (int j = 0; j < stolenVehs[i].possibleCallers.Length; j++)
                                {
                                    Yield();

                                    if (stolenVehs[i].possibleCallers[j] != null && stolenVehs[i].possibleCallers[j].Exists())
                                    {
                                        if (stolenVehs[i].possibleCallers[j].IsAlive)
                                        {
                                            called = true;
                                            break;
                                        }
                                    }
                                    else called = stolenVehs[i].possibleCallersHaveCalled; //if callers were alive last time checked then called = true
                                }

                                stolenVehs[i].possibleCallersHaveCalled = called;
                            }
                        }
                    }
                    else timeWithinDistanceCops = 0;

                    Ped originalDriver;
                    Vehicle vehicleBeingStolen;

                    if (isStealingVehicle(plrPed, out vehicleBeingStolen, out originalDriver) && !stolenVehs.ContainsVehicle(vehicleBeingStolen))
                    {
                        while (plrPed.IsGettingIntoAVehicle | Function.Call<bool>(Hash.GET_IS_TASK_ACTIVE, plrPed, 165)) Wait(100); //165 = switching seats

                        if (plrPed.IsInVehicle(vehicleBeingStolen) && plrPed.SeatIndex == VehicleSeat.Driver)
                        {
                            if (INI.settings.showExtraInfoMessages) UI.Notify("In stolen vehicle");

                            List<Ped> possibleCallers = new List<Ped>();

                            if (originalDriver != null && originalDriver.Exists() && originalDriver.IsAlive) possibleCallers.Add(originalDriver);

                            if (vehicleBeingStolen.PassengerCount > 0) possibleCallers.AddRange(vehicleBeingStolen.Passengers.Where(x => x != null && x.Exists() && x.IsAlive));

                            bool copsSeeMe = false;

                            foreach (Ped ped in getNearbyHumansLookingAtEntity(plrPed, vehicleBeingStolen, INI.settings.callersSearchRadius))
                            {
                                if (copsPedTypes.Contains(Function.Call<int>(Hash.GET_PED_TYPE, ped))) //is cop
                                {
                                    copsSeeMe = true;
                                    break;
                                }

                                ped.IsPersistent = true;
                                possibleCallers.Add(ped);
                            }

                            if (copsSeeMe) //just set wanted level now
                            {
                                if (INI.settings.showCopsSawYouStealingMessage ||
                                    INI.settings.showExtraInfoMessages) UI.Notify(INI.settings.copsSawYouStealingMessage);

                                foreach (Ped ped in possibleCallers)
                                {
                                    ped.IsPersistent = false;
                                }

                                if (INI.settings.receiveAdditionalStarWhenWanted && plr.WantedLevel > 0)
                                {
                                    if (INI.settings.showWantedAlreadyCopsFoundYouMessage || INI.settings.showExtraInfoMessages) UI.Notify(INI.settings.wantedAlreadyCopsFoundYouMessage);

                                    if (plr.WantedLevel < INI.settings.maxAdditionalStars) plr.WantedLevel++;
                                }
                                else if (plr.WantedLevel < INI.settings.starsOnCall) plr.WantedLevel = INI.settings.starsOnCall;
                            }
                            else
                            {
                                if (INI.settings.showExtraInfoMessages) UI.Notify("Number of callers: " + possibleCallers.Count.ToString());

                                bool anyPossibleCallers = possibleCallers.Count != 0;

                                if ((!anyPossibleCallers && INI.settings.starsOnNoticeStolenCar != 0) ||
                                    (anyPossibleCallers && INI.settings.starsOnCall != 0))
                                {
                                    stolenVehs.Add(new StolenVehicle(vehicleBeingStolen, anyPossibleCallers, possibleCallers.ToArray()));
                                }
                                else if (INI.settings.showExtraInfoMessages) UI.Notify("Stars on call or stars on notice stolen vehicle is disabled");
                            }
                        }
                    }
                }
                else
                {
                    stolenVehs.Clear();
                    timeWithinDistanceCops = 0;
                }
            }

            Monitor.Exit(objTimerLock);
        }

        private void GrandTheftAuto_Tick(object sender, EventArgs e)
        {
            processStolenVehicles();
        }
    }
}
