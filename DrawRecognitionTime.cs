﻿using GTA;
using System;
using System.Drawing;

namespace GrandTheftAuto
{
    class GTA_Draw : Script
    {
        UIText recognitionMessage = null;
        int lastDrawnTimeWithinDistanceCops = 0;

        public GTA_Draw()
        {
            Interval = 0;
            Tick += GTA_Draw_Tick;
        }

        private void GTA_Draw_Tick(object sender, EventArgs e)
        {
            if (GrandTheftAuto.timeWithinDistanceCops > 0 && GrandTheftAuto.stolenVehs.Count > 0)
            {
                if (recognitionMessage == null) recognitionMessage = new UIText("1", new Point(INI.settings.recognitionMessagePosX, INI.settings.recognitionMessagePosY), INI.settings.recognitionMessageScale);

                if (lastDrawnTimeWithinDistanceCops != GrandTheftAuto.timeWithinDistanceCops)
                {
                    lastDrawnTimeWithinDistanceCops = GrandTheftAuto.timeWithinDistanceCops;
                    recognitionMessage.Caption = GrandTheftAuto.timeWithinDistanceCops.ToString();
                }

                recognitionMessage.Draw();
            }
        }
    }
}
