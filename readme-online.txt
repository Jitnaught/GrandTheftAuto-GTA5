﻿Yes, a mod for Grand Theft Auto V called Grand Theft Auto.

With this mod, when you steal a vehicle, pedestrians around you will report you to the cops. If there are no pedestrians (or they are dead within around 15 seconds), then a missing vehicle will be reported in around 5 minutes.
When the police have been notified, they will search for you. If they see you and fully recognize that it is you, you will get a wanted level.

Installation:
<ul><li>Install Script Hook V and Script Hook V .NET (make sure you have created a scripts folder in your GTA V directory).</li>
<li>Extract the GrandTheftAuto.dll and GrandTheftAuto.ini files into the scripts folder in your GTA V directory.</li></ul>

Description of each INI setting in the readme.txt file.
More on how the mod works in the howitworks.txt file.

Known issues:
<ul><li>There may be some lag.</li>
<li>Some vehicles that aren't stolen may be picked up, and some that are stolen may not be picked up.</li></ul>

<b>Change-log:</b>
v1.0.1:
<ul><li>Fixed changing the search radius settings not having any effect.</li></ul>
<a href="https://github.com/Jitnaught/GrandTheftAuto_Mod">See the source code on GitHub!</a>

Credits:
<ul><li>destroyer11687 (a.k.a. /u/NuttyWompRat of Reddit) for requesting the mod. (https://www.reddit.com/r/GTAV_Mods/comments/5745mj/would_anybody_be_willing_to_teach_me_how_to/)</li>
<li>Jitnaught for writing the script.</li></ul>